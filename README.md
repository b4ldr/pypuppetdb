# pypuppetdb

[![PyPi Version](https://img.shields.io/pypi/v/pypuppetdb)](https://pypi.org/project/pypuppetdb/)
[![PyPi Downloads](https://img.shields.io/pypi/dm/pypuppetdb)](https://pypi.org/project/pypuppetdb/)
![Tests Status](https://github.com/voxpupuli/pypuppetdb/workflows/tests%20(unit)/badge.svg)
[![Coverage Status](https://img.shields.io/coveralls/voxpupuli/pypuppetdb.svg)](https://coveralls.io/r/voxpupuli/pypuppetdb)
[![Documentation Status](https://readthedocs.org/projects/pypuppetdb/badge/?version=latest)](https://pypuppetdb.readthedocs.io/en/latest/?badge=latest)
[![By Voxpupuli](https://img.shields.io/badge/by-Vox%20Pupuli%20%F0%9F%A6%8A-ef902f.svg)](http://voxpupuli.org)

This library is a thin wrapper around the REST API providing some
convenience functions and objects to request and hold data from
PuppetDB.

## Requirements

* PuppetDB 3.0 or newer
  * (For support of older PuppetDB versions please check versions < 0.2.0)
* Python 3.6 / 3.7 / 3.8 / 3.9

## Installation

You can install this package from PyPI with:

```bash
pip install pypuppetdb
```

## Documentation

The [user guide, API reference and a developer guide is available on Read the Docs](https://pypuppetdb.readthedocs.io/en/latest/).

## Getting Help

For bug reports you can file an
[issue](https://github.com/voxpupuli/pypuppetdb/issues). If you need
help with something feel free to pop by \#voxpupuli or the \#puppetboard on
[Freenode](http://freenode.net) or [Vox Pupuli on Slack](https://puppetcommunity.slack.com/messages/voxpupuli/).

## Contributing

Please see the [contribution guide here](https://github.com/voxpupuli/pypuppetdb/blob/master/CONTRIBUTING.md).

## License

This project is licensed under the [Apache v2.0 License](https://github.com/voxpupuli/pypuppetdb/blob/master/LICENSE).
